#!/bin/bash

gp ports await 23000

cp /workspace/remote-dev/id_rsa{,.pub} ~/.ssh

sudo apt install -y postgresql tmux

pip install all-repos

if [ ! -f ngrok-v3-stable-linux-amd64.tgz ]; then
  wget https://bin.equinox.io/c/bNyj1mQVY4c/ngrok-v3-stable-linux-amd64.tgz
fi
sudo tar xvzf ngrok-v3-stable-linux-amd64.tgz -C /usr/local/bin

_exts=(
  ms-python.python
  eamodio.gitlens
  GitHub.copilot
  redhat.vscode-yaml
  ms-azuretools.vscode-docker
  hashicorp.terraform
  redhat.vscode-xml
  esbenp.prettier-vscode
)
for _ext in "${_exts[@]}"; do {
   code --install-extension "$_ext";
} done
